const mongoose = require('mongoose');
const { tokenTypes } = require('../config/tokens');

const tokenSchema=new mongoose.Schema({
    token:{
        type:String,
        required:true,
        index:true
    },
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        required: true
    },
    type:{
        type:String,
        enum:[tokenTypes.REFRESH,tokenTypes.RESET_PASSWORD,tokenTypes.VERIFY_EMAIL],
        required:true
    },
    expires:{
        type:Date,
        required:true,
    },
    blacklisted:{
        type:Boolean,
        default:false
    }
},{timestamp:true});

//here we need to add plugins


const Token=mongoose.model('Token',tokenSchema);
module.exports=Token;